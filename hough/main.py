import math
import numpy as np
import cv2
import scipy.ndimage.filters as filters


# Circle Hough Transform https://en.wikipedia.org/wiki/Circle_Hough_Transform
def cht(filename, Rmin, Rmax, thrCanny1, thrCanny2):
    # Read image
    img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    rows = img.shape[0]
    cols = img.shape[1]

    # Detect edges with Canny, thresholds are given as parameters for each image
    edges = cv2.Canny(img, thrCanny1, thrCanny2)

    # Nonzero elements of edge map
    E = np.nonzero(edges)
    # Accumulator matrix
    acc = np.zeros((rows + 2 * Rmax, cols + 2 * Rmax, Rmax - Rmin + 1))

    # Prepare matrix for shifting edges by radii, idea comes from
    # https://www.mathworks.com/matlabcentral/fileexchange/72998-circles-detection-using-hough-transform
    X, Y = np.meshgrid(range(2 * Rmax), range(2 * Rmax))
    R = np.round(np.sqrt((X - Rmax) ** 2 + (Y - Rmax) ** 2))
    R[np.logical_or(R < Rmin, R > Rmax)] = 0
    Cx, Cy = np.nonzero(R)

    # Fill the accumulator by shifting
    for x, y in zip(*E):
        for cx, cy in zip(*(Cx, Cy)):
            r = int(R[cx, cy])
            acc[cx + x - 1, cy + y - 1, r - Rmin] += 1 / (2 * r * np.pi)

    circle_groups = {}
    neighborhood_size = Rmin
    for i in range(Rmax - Rmin):
        curr_acc = acc[:, :, i]
        # strengthen local maxima
        filtered_acc = curr_acc * (curr_acc == filters.maximum_filter(curr_acc, neighborhood_size))
        filtered_acc[filtered_acc < 0.33] = 0

        # group circles by closeness of center point
        for x, y in zip(*(np.nonzero(filtered_acc))):
            c1 = (y - Rmax, x - Rmax, i + Rmin)
            min_d = math.inf
            min_k = -1
            for k, v in circle_groups.items():
                for c2 in v:
                    d = math.sqrt((c1[0] - c2[0]) ** 2 + (c1[1] - c2[1]) ** 2)
                    if d < min_d and d < Rmin:
                        min_d = d
                        min_k = k
            if min_k == -1:
                circle_groups[len(circle_groups)] = [c1]
            else:
                circle_groups[min_k].append(c1)

    # average groups of circles to get strong, individual circles
    circles = []
    for k, v in circle_groups.items():
        sum_x = 0
        sum_y = 0
        sum_r = 0
        for c in v:
            sum_x += c[0]
            sum_y += c[1]
            sum_r += c[2]
        circles.append((sum_x // len(v), sum_y // len(v), sum_r // len(v)))

    # draw circles on original image
    img2 = cv2.imread(filename, cv2.IMREAD_COLOR)
    for c in circles:
        cv2.circle(img2, (c[0], c[1]), c[2], (0, 0, 255), 1)
        cv2.rectangle(img2, (c[0], c[1]), (c[0], c[1]), (0, 0, 255), 1)

    # sum and normalize accumulator
    acc = np.sum(acc, 2)
    # acc = acc / np.max(acc)
    acc *= (255 // np.max(acc))

    # show images for testing
    # cv2.imshow("Img", img)
    # cv2.imshow("Edges", edges)
    # cv2.imshow("Acc", acc)
    # cv2.imshow("Circles", img2)
    # cv2.waitKey()

    # write output images
    cv2.imwrite('accumulator_' + filename, acc)
    cv2.imwrite('detected_circles_' + filename, img2)

    print(f'Finished CHT, image: {filename}')


if __name__ == '__main__':
    cht('blood.png', 11, 14, 130, 170)
    cht('cable.png', 20, 25, 80, 140)
    cht('cells.png', 10, 15, 60, 100)
    cht('circles.png', 12, 14, 130, 170)

import numpy as np
import cv2


def otsu(filename):
    # Read image
    img = cv2.imread(filename, 0)

    P, _ = np.histogram(img, 256, [0, 256], density=True)

    mu = np.mean(img)
    q1 = 0
    mu1 = 0
    max_sigma_b2 = -1
    threshold = -1
    for t in range(255):
        # recursive eq 2
        next_q1 = q1 + P[t+1]

        mu1 = (q1 * mu1 + (t+1) * P[t+1]) / next_q1 if next_q1 != 0 else 0

        q1 = next_q1

        mu2 = (mu - q1 * mu1) / (1 - q1) if q1 != 1 else 0

        sigma_b2 = q1 * (1 - q1) * ((mu1 - mu2) ** 2)

        if sigma_b2 > max_sigma_b2:
            max_sigma_b2 = sigma_b2
            threshold = t

    result = np.copy(img)
    result[img > threshold] = 255
    result[img < threshold] = 0

    # write output images
    cv2.imwrite('thresholded_' + filename, result)

    print(f'Finished Otsu thresholding, image: {filename}, threshold: {threshold}')


if __name__ == '__main__':
    otsu('aluminium.png')
    otsu('finger.png')
    otsu('julia.png')
    otsu('phobos.png')

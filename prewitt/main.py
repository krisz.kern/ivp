import numpy as np
import cv2
import time
import math

# Kernels
Px = np.array([[-1, 0, 1],
               [-1, 0, 1],
               [-1, 0, 1]])

Py = np.array([[1, 1, 1],
               [0, 0, 0],
               [-1, -1, -1]])

# 8 discrete angles from -180 to 135
D_A_map = {
    -180: ((0, -1), (0, +1)),
    -135: ((+1, -1), (-1, +1)),
    -90: ((-1, 0), (+1, 0)),
    -45: ((-1, -1), (+1, +1)),
    0: ((0, -1), (0, +1)),
    45: ((+1, -1), (-1, +1)),
    90: ((-1, 0), (+1, 0)),
    135: ((-1, -1), (+1, +1)),
}

D_A = list(D_A_map.keys())


def prewitt(filename='circlegrey.png'):
    print(f'Starting Prewitt edge detection + NMS for file: {filename}')
    start = time.time()

    # Read image
    img = cv2.imread(filename, 0)
    padded_img = np.pad(img, 1)

    # Magnitudes and angles
    M = np.zeros(img.shape)
    A = np.zeros(img.shape)
    for i, j in np.ndindex(M.shape):
        r_gx, r_gy = 0, 0
        for k, l in np.ndindex(Px.shape):
            r_gx += Px[k, l] * padded_img[i + k - 1, j + l - 1]
            r_gy += Py[k, l] * padded_img[i + k - 1, j + l - 1]
        m = math.sqrt(r_gx ** 2 + r_gy ** 2)
        M[i, j] = m if m > 0 else 0
        A[i, j] = math.atan2(r_gy, r_gx) / math.pi * 180

    # rounded angles
    R_A = np.zeros(A.shape)
    for i, j in np.ndindex(A.shape):
        R_A[i, j] = D_A[np.array([abs(a - A[i, j]) for a in D_A]).argmin()]

    # Non-maxima suppression
    # padded magnitudes
    P_M = np.pad(M, 1)
    # suppressed img
    S = np.zeros(M.shape)

    for i, j in np.ndindex(S.shape):
        C = D_A_map[R_A[i, j]]
        if not (P_M[i + 1, j + 1] < P_M[i + 1 + C[0][0], j + 1 + C[0][1]] or
                P_M[i + 1, j + 1] < P_M[i + 1 + C[1][0], j + 1 + C[1][1]]):
            S[i, j] = P_M[i + 1, j + 1]

    # write output images
    cv2.imwrite('magnitudes_' + filename, M)
    cv2.imwrite('suppressed_' + filename, S)

    end = time.time()
    print('Finished edge detection, magnitudes and suppressed image written')
    print(f'Elapsed time: {end - start:.3f} seconds')


if __name__ == '__main__':
    prewitt('circlegrey.png')
    prewitt('julia.png')
    prewitt('motor.png')

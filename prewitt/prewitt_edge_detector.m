function [magnitudes, rounded] = prewitt_edge_detector(image, name)

x_kernel = [...
    -1 0 1; ...
    -1 0 1; ...
    -1 0 1];
y_kernel = [...
    1 1 1; ...
    0 0 0; ...
    -1 -1 -1];

[~, I_dx] = correlation_filtering(image, x_kernel);
[~, I_dy] = correlation_filtering(image, y_kernel);

magnitudes = I_dx.^2 + I_dy.^2;
imwrite(magnitudes, strcat(name, '_magnitudes.png'));

angles = atan2(I_dy, I_dx) / pi * 180;
angles = angles + (angles < 0) * 180;

rounded = angle_rounding(angles);
edges = nonmaxima_supression(magnitudes, rounded);

figure, imshow([image, magnitudes, edges]);
imwrite(edges, strcat(name, '_nonmax.png'));
end

function [supressed] = nonmaxima_supression(magnitudes, rounded)
image_dims = size(magnitudes);

magnitudes_padded = zeros(1 + image_dims(1) + 1, 1 + image_dims(2) + 1);
magnitudes_padded(2 : 1 + image_dims(1), 2 : 1 + image_dims(2)) = magnitudes;
supressed = zeros(image_dims(1), image_dims(2));

for i = 2 : 1 + image_dims(1)
    for j = 2 : 1 + image_dims(2)
        
        supress = 0;
        switch rounded(i - 1, j - 1)
            case 1 % approx 0
                supress = (...
                    magnitudes_padded(i, j) < magnitudes_padded(i, j - 1) || ...
                    magnitudes_padded(i, j) < magnitudes_padded(i, j + 1));
            case 2 % approx 22.5
                supress = (...
                    magnitudes_padded(i, j) < magnitudes_padded(i, j - 1) || ...
                    magnitudes_padded(i, j) < magnitudes_padded(i - 1, j + 1));
            case 3 % approx 45
                supress = (...
                    magnitudes_padded(i, j) < magnitudes_padded(i + 1, j - 1) || ...
                    magnitudes_padded(i, j) < magnitudes_padded(i - 1, j + 1));
            case 4 % approx 67.5
                supress = (...
                    magnitudes_padded(i, j) < magnitudes_padded(i + 1, j - 1) || ...
                    magnitudes_padded(i, j) < magnitudes_padded(i - 1, j));
            case 5 % approx 90
                supress = (...
                    magnitudes_padded(i, j) < magnitudes_padded(i + 1, j) || ...
                    magnitudes_padded(i, j) < magnitudes_padded(i - 1, j));
            case 6 % approx 112.5
                supress = (...
                    magnitudes_padded(i, j) < magnitudes_padded(i + 1, j) || ...
                    magnitudes_padded(i, j) < magnitudes_padded(i - 1, j - 1));
            case 7 % approx 135
                supress = (...
                    magnitudes_padded(i, j) < magnitudes_padded(i + 1, j + 1) || ...
                    magnitudes_padded(i, j) < magnitudes_padded(i - 1, j - 1));
            case 8 % approx 157.5
                supress = (...
                    magnitudes_padded(i, j) < magnitudes_padded(i + 1, j + 1) || ...
                    magnitudes_padded(i, j) < magnitudes_padded(i, j - 1));
        end
        if ~supress
            supressed(i - 1, j - 1) = magnitudes_padded(i, j);
        end
    end
end
end

% round edges from the interval [-90, 90] to discrete values of [1 : 8]
function [rounded] = angle_rounding(angles)
image_dims = size(angles);
rounded = zeros(image_dims(1), image_dims(2));

for i = 1 : image_dims(1)
    for j = 1 : image_dims(2)
        if angles(i, j) < 11.25
            rounded(i, j) = 1;
        elseif 11.25 <= angles(i, j) && angles(i, j) < 33.75
            rounded(i, j) = 2;
        elseif 33.75 <= angles(i, j) && angles(i, j) < 56.25
            rounded(i, j) = 3;
        elseif 56.25 <= angles(i, j) && angles(i, j) < 78.75
            rounded(i, j) = 4;
        elseif 78.75 <= angles(i, j) && angles(i, j) < 101.25
            rounded(i, j) = 5;
        elseif 101.25 <= angles(i, j) && angles(i, j) < 123.75
            rounded(i, j) = 6;
        elseif 123.75 <= angles(i, j) && angles(i, j) < 146.25
            rounded(i, j) = 7;
        elseif 146.25 <= angles(i, j) && angles(i, j) < 168.75
            rounded(i, j) = 8;
        else
            rounded(i, j) = 1;
        end
    end
end
end

% correlation works on odd-odd(!) kernels
function [padded, filtered] = correlation_filtering(image, kernel)

kernel_dims = size(kernel);
image_dims = size(image);

filtered = zeros(image_dims(1), image_dims(2));
padded = zero_padding(image, kernel);

% the outer loops fill the filtered image
for i = 1 : image_dims(1)
    for j = 1 : image_dims(2)
        
        % the inner loops calculate one operation
        for x = 1 : kernel_dims(1)
            for y = 1 : kernel_dims(2)
                
                % indexing works because of padding (this surprises me as well O.O )
                filtered(i, j) = filtered(i, j) + ...
                    padded(i + x - 1, j + y - 1) * kernel(x, y);
            end
        end
        
        % end of the inner loop
    end
end

end

% padding works on odd-odd(!) sized kernels
function [padded] = zero_padding(image, kernel)

kernel_dims = size(kernel);
image_dims = size(image);
top_bottom_extension = (kernel_dims(1) - 1) / 2;
left_right_extension = (kernel_dims(2) - 1) / 2;

% create extension in each direction
padded = zeros(...
    top_bottom_extension + image_dims(1) + top_bottom_extension, ...
    left_right_extension + image_dims(2) + left_right_extension);

% insert original image into the middles
padded(...
    top_bottom_extension + 1 : top_bottom_extension + image_dims(1), ...
    left_right_extension + 1 : left_right_extension + image_dims(2)) = image;
end
close all, clear all, clc;

I = im2double(imread('circlegrey.png'));
[M, E] = prewitt_edge_detector(I, 'circlegrey');

I = im2double(imread('julia.png'));
[M, E] = prewitt_edge_detector(I, 'julia');

I = im2double(imread('motor.png'));
[M, E] = prewitt_edge_detector(I, 'motor');